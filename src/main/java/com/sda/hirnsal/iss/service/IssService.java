package com.sda.hirnsal.iss.service;

import com.sda.hirnsal.iss.dto.Person;
import com.sda.hirnsal.iss.dto.Position;
import java.util.List;

public interface IssService {

    List<Person> getAstronauts();
    Position getIssPosition();

}
