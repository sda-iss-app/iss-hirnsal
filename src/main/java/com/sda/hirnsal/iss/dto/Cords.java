package com.sda.hirnsal.iss.dto;

import lombok.Data;

@Data
public class Cords {

    private Float longitude;
    private Float latitude;

}
